package com.atlassian.platform.maven;

import org.custommonkey.xmlunit.XMLUnit;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.junit.Test;

import java.io.InputStream;

import static org.custommonkey.xmlunit.XMLAssert.assertXMLEqual;

public class PlatformChecksTest {

    @Test
    public void testAddPlugin() throws Exception {
        InputStream atlassianPluginXml = getClass().getResourceAsStream("/platform-checks/atlassian-plugin.xml");

        // Create output document
        Document document = DocumentHelper.createDocument();
        Element root = document.addElement("platform");
        root.addAttribute("version", "3.0.6");

        PlatformChecks platformChecks = new PlatformChecks(root);

        platformChecks.addPlugin(atlassianPluginXml);

        SAXReader reader = new SAXReader();
        Document expected = reader.read(getClass().getResourceAsStream("/platform-checks/platformversions.xml"));

        XMLUnit.setIgnoreWhitespace(true);
        assertXMLEqual(document.asXML(), expected.asXML());
    }
}