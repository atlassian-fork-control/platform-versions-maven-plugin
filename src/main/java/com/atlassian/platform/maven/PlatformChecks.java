package com.atlassian.platform.maven;

import aQute.bnd.header.Attrs;
import aQute.bnd.header.OSGiHeader;
import aQute.bnd.header.Parameters;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.osgi.framework.VersionRange;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.jar.Manifest;

class PlatformChecks {
    private final Element docRoot;

    public PlatformChecks(Element docRoot) {
        this.docRoot = docRoot;
    }

    public void addPlugin(InputStream in) throws DocumentException {
        SAXReader reader = new SAXReader();
        Document doc = reader.read(in);

        String key = doc.valueOf("/atlassian-plugin/@key");
        String name = doc.valueOf("/atlassian-plugin/@name");
        String version = doc.valueOf("/atlassian-plugin/plugin-info/version/text()");

        Element test = docRoot.addElement("plugin-version-check");
        test.addAttribute("key", key);
        test.addAttribute("version", version);
        test.addAttribute("modulename", name);
    }

    public void addService(InputStream in) throws IOException {
        Manifest m = new Manifest(in);
        String exports = m.getMainAttributes().getValue("Export-Package");
        if (exports == null || exports.length() == 0) {
            return;
        }

        Parameters p = OSGiHeader.parseHeader(exports);
        for (Map.Entry<String, Attrs> pkg : p.entrySet()) {
            String version = pkg.getValue().get("version");
            if (version == null) {
                continue;
            }

            VersionRange range = new VersionRange(version);

            Element test = docRoot.addElement("export-version-check");
            test.addAttribute("package", pkg.getKey());
            test.addAttribute("version", range.getLeft().toString());
        }
    }

}
